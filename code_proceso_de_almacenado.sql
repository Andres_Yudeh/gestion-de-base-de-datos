DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `citas_niñeros`(IN `NAME` VARCHAR(250))
SELECT ninero.NOMBRE_NI AS Nombre_Niñero , YEAR(solicitud.FECHA_INGRESO) AS AÑO, COUNT(solicitud.ESTADO_DE_SOLICITUD) AS CANTIDAD FROM `solicitud` INNER JOIN `ninero` WHERE solicitud.ID_NINERO = ninero.ID_NINERO AND solicitud.ESTADO_DE_SOLICITUD = "1" AND ninero.NOMBRE_NI LIKE CONCAT('%',NAME,'%') GROUP BY solicitud.ID_NINERO , YEAR(solicitud.FECHA_INGRESO)$$
DELIMITER ;