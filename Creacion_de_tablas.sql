-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-11-2021 a las 06:11:51
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `quinto`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `citas_niñeros` (IN `NAME` VARCHAR(250))  SELECT ninero.NOMBRE_NI AS Nombre_Niñero , YEAR(solicitud.FECHA_INGRESO) AS AÑO, COUNT(solicitud.ESTADO_DE_SOLICITUD) AS CANTIDAD FROM `solicitud` INNER JOIN `ninero` WHERE solicitud.ID_NINERO = ninero.ID_NINERO AND solicitud.ESTADO_DE_SOLICITUD = "1" AND ninero.NOMBRE_NI LIKE CONCAT('%',NAME,'%') GROUP BY solicitud.ID_NINERO , YEAR(solicitud.FECHA_INGRESO)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristica_de_nino`
--

CREATE TABLE `caracteristica_de_nino` (
  `ENFERMEDADES` char(250) NOT NULL,
  `ALERGIAS` char(250) NOT NULL,
  `DISCAPACIDADES` char(250) NOT NULL,
  `INDICACIONES` char(250) NOT NULL,
  `CEDULA_NINO` char(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `ID_CLIENTE` int(10) NOT NULL,
  `CEDULA` char(250) NOT NULL,
  `CEDULA_NINO` char(250) NOT NULL,
  `NOMBRE` char(250) NOT NULL,
  `APELLIDO` char(250) NOT NULL,
  `IDEOLOGIA` char(250) NOT NULL,
  `CORREO` char(250) NOT NULL,
  `CELL_1` text NOT NULL,
  `CELL_2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--


--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `DIRECCION` char(250) NOT NULL,
  `ID_CLIENTE` int(10) NOT NULL,
  `CEDULA` char(250) NOT NULL,
  `CIUDAD` char(250) NOT NULL,
  `TELEFONO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `domicilio`
--


--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `ID_FACTURA` char(250) NOT NULL,
  `ID_PAGO` char(250) NOT NULL,
  `MONTO_DE_PAGO` int(10) DEFAULT NULL,
  `FECHA_DE_PAGO` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura`
--



--
-- Estructura de tabla para la tabla `ninero`
--

CREATE TABLE `ninero` (
  `ID_NINERO` char(250) NOT NULL,
  `NOMBRE_NI` char(250) NOT NULL,
  `APELLIDO_NI` char(250) NOT NULL,
  `CEDULA_NI` char(250) NOT NULL,
  `IDEOLOGIA_NI` char(250) NOT NULL,
  `NACIMIENTO` date NOT NULL,
  `CORREO_NI` char(250) NOT NULL,
  `CELL_NI` varchar(10) NOT NULL,
  `VALOR_POR_HORA` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--

--
-- Estructura de tabla para la tabla `nino`
--

CREATE TABLE `nino` (
  `CEDULA_NINO` char(250) NOT NULL,
  `NOMBRE_NINO` char(250) NOT NULL,
  `APELLIDO_NINO` char(250) NOT NULL,
  `NACIMIENTO_NINO` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--


--
-- Estructura de tabla para la tabla `ocupacion_ninero`
--

CREATE TABLE `ocupacion_ninero` (
  `ID_NINERO` char(250) NOT NULL,
  `AFICION` char(250) NOT NULL,
  `CEDULA_NI` char(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--


--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `ID_PAGO` char(250) NOT NULL,
  `METODO_DE_PAGO` char(250) NOT NULL,
  `MONTO_DE_PAGO` int(10) NOT NULL,
  `FECHA_DE_PAGO` date NOT NULL,
  `ID_SOLICITUD` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--


--
-- Estructura de tabla para la tabla `referencias`
--

CREATE TABLE `referencias` (
  `REFERENCIA_NI` char(250) NOT NULL,
  `ID_NINERO` char(250) NOT NULL,
  `CEDULA_NI` char(250) NOT NULL,
  `RF_AÑO` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--


--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE `solicitud` (
  `ID_SOLICITUD` char(250) NOT NULL,
  `FECHA_INGRESO` datetime NOT NULL,
  `FECHA_SALIDA` datetime NOT NULL,
  `ESTADO_DE_SOLICITUD` tinyint(1) NOT NULL,
  `OBSERVACIONES` char(250) NOT NULL,
  `ID_CLIENTE` int(10) NOT NULL,
  `CEDULA` char(250) NOT NULL,
  `ID_NINERO` char(250) NOT NULL,
  `CEDULA_NI` char(250) NOT NULL,
  `CALIFICACION` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--


--

--
