-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-11-2021 a las 06:11:51
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `quinto`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `citas_niñeros` (IN `NAME` VARCHAR(250))  SELECT ninero.NOMBRE_NI AS Nombre_Niñero , YEAR(solicitud.FECHA_INGRESO) AS AÑO, COUNT(solicitud.ESTADO_DE_SOLICITUD) AS CANTIDAD FROM `solicitud` INNER JOIN `ninero` WHERE solicitud.ID_NINERO = ninero.ID_NINERO AND solicitud.ESTADO_DE_SOLICITUD = "1" AND ninero.NOMBRE_NI LIKE CONCAT('%',NAME,'%') GROUP BY solicitud.ID_NINERO , YEAR(solicitud.FECHA_INGRESO)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristica_de_nino`
--

CREATE TABLE `caracteristica_de_nino` (
  `ENFERMEDADES` char(250) NOT NULL,
  `ALERGIAS` char(250) NOT NULL,
  `DISCAPACIDADES` char(250) NOT NULL,
  `INDICACIONES` char(250) NOT NULL,
  `CEDULA_NINO` char(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `caracteristica_de_nino`
--

INSERT INTO `caracteristica_de_nino` (`ENFERMEDADES`, `ALERGIAS`, `DISCAPACIDADES`, `INDICACIONES`, `CEDULA_NINO`) VALUES
('NA', 'NA', 'NA', 'El niño es muy travieso, estar pendiente porfavor.', '1302587453'),
('NA', 'NA', 'Síndrome de Down', 'Estar muy al pendiente porfavor.', '1306321749'),
('NA', 'NA', 'NA', 'No brindarle dulces. Los tiene prohibidos.', '1319852015'),
('NA', 'Camarones', 'NA', 'No darle camarones.', '1310147851'),
('NA', 'Lácteos', 'NA', 'No darle productos que contengan lácteos.', '1247965712'),
('Bronquitis aguda', 'NA', 'NA', 'No darle productos que estén helados', '1300258796'),
('NA', 'NA', 'NA', 'No dejar dormir, porque después no duerme por la noche.', '1312036498'),
('Gripe', 'NA', 'NA', 'No dejar que consuma productos helados', '1305411785');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `ID_CLIENTE` int(10) NOT NULL,
  `CEDULA` char(250) NOT NULL,
  `CEDULA_NINO` char(250) NOT NULL,
  `NOMBRE` char(250) NOT NULL,
  `APELLIDO` char(250) NOT NULL,
  `IDEOLOGIA` char(250) NOT NULL,
  `CORREO` char(250) NOT NULL,
  `CELL_1` text NOT NULL,
  `CELL_2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`ID_CLIENTE`, `CEDULA`, `CEDULA_NINO`, `NOMBRE`, `APELLIDO`, `IDEOLOGIA`, `CORREO`, `CELL_1`, `CELL_2`) VALUES
(1, '1316541239', '1247965712', 'Pedro Jose', 'Cedeño Quijije', 'Heterosexual', 'pedroce@hotmail.com', '0921478536', '0932145207'),
(2, '1336520139', '1300258796', 'Ian Jesus', 'Anchundia Bailon', 'Heterosexual', 'ibaba@hotmail.com', '0927530197', '0901874121'),
(3, '1318052147', '1302587453', 'Martha Mishell', 'Piguave Zambrano', 'Heterosexual', 'marpi@hotmail.com', '0995103790', '0946791301'),
(4, '1310851374', '1305411785', 'Jesus Manuel', 'Bailon Palma', 'Heterosexual', 'jeba@hotmail.com', '0965123078', '0924652138'),
(5, '1312136874', '1306321749', 'Ana María', 'Guzmán Mero', 'Heterosexual', 'anguz@hotmail.com', '0952149512', '0962142039'),
(6, '1306541202', '1310147851', 'Carlos Andres', 'Santos Mera', 'Heterosexual', 'carsans@hotmail.com', '0930145287', '0974125471'),
(7, '1312547842', '1312036498', 'Laddy Mabel', 'Anchundia Velez', 'Heterosexual', 'laddve@hotmail.com', '0908546978', '0931245712'),
(8, '1325874142', '1319852015', 'Jonathan Francisco', 'Palma Alvarado', 'Heterosexual', 'jonapal@hotmail.com', '0965412078', '0995135742');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `DIRECCION` char(250) NOT NULL,
  `ID_CLIENTE` int(10) NOT NULL,
  `CEDULA` char(250) NOT NULL,
  `CIUDAD` char(250) NOT NULL,
  `TELEFONO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `domicilio`
--

INSERT INTO `domicilio` (`DIRECCION`, `ID_CLIENTE`, `CEDULA`, `CIUDAD`, `TELEFONO`) VALUES
('Barrio jocay, entre calle 4 y avenida 25.', 3, '1318052147', 'Manta', 534782),
('Barrio La Aurora, calle C-3.', 5, '1312136874', 'Manta', 528974),
('Barrio La Pradera, entre calle C-9 y pasaje 2', 6, '1306541202', 'Manta', 574136),
('Barrio Las Acacias, calle 16 y avenida 36.', 8, '1325874142', 'Manta', 587419),
('Barrio las Fragatas, calle 2 y avenida 124', 7, '1312547842', 'Manta', 521478),
('barrio San Pedro entre calles 3 y avenida 29', 1, '1316541239', 'Manta', 592315),
('Calle universitaria, avenida 32.', 4, '1310851374', 'Manta', 574279),
('Via interbarrial, calle 326.', 2, '1336520139', 'Manta', 512798);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `ID_FACTURA` char(250) NOT NULL,
  `ID_PAGO` char(250) NOT NULL,
  `MONTO_DE_PAGO` int(10) DEFAULT NULL,
  `FECHA_DE_PAGO` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`ID_FACTURA`, `ID_PAGO`, `MONTO_DE_PAGO`, `FECHA_DE_PAGO`) VALUES
('FT01', 'PG01', 8, '2021-02-15'),
('FT02', 'PG02', 20, '2021-02-17'),
('FT03', 'PG03', 12, '2021-03-04'),
('FT04', 'PG04', 28, '2021-03-14'),
('FT05', 'PG05', 65, '2021-03-23'),
('FT06', 'PG06', 42, '2021-04-01'),
('FT07', 'PG07', 20, '2021-04-04'),
('FT08', 'PG08', 32, '2021-04-11'),
('FT09', 'PG09', 40, '2021-04-23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ninero`
--

CREATE TABLE `ninero` (
  `ID_NINERO` char(250) NOT NULL,
  `NOMBRE_NI` char(250) NOT NULL,
  `APELLIDO_NI` char(250) NOT NULL,
  `CEDULA_NI` char(250) NOT NULL,
  `IDEOLOGIA_NI` char(250) NOT NULL,
  `NACIMIENTO` date NOT NULL,
  `CORREO_NI` char(250) NOT NULL,
  `CELL_NI` varchar(10) NOT NULL,
  `VALOR_POR_HORA` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ninero`
--

INSERT INTO `ninero` (`ID_NINERO`, `NOMBRE_NI`, `APELLIDO_NI`, `CEDULA_NI`, `IDEOLOGIA_NI`, `NACIMIENTO`, `CORREO_NI`, `CELL_NI`, `VALOR_POR_HORA`) VALUES
('NI01', 'Paula Andrea', 'Cardenas Cedeño', '1315147038', 'Heterosexual', '1984-09-11', 'paucardenas@hotmail.com', '0942130147', 5),
('NI02', 'Carlos Abel', 'Zambrano Macias', '1310149624', 'Heterosexual', '1999-05-02', 'carzam@hotmail.com', '0925841369', 4),
('NI03', 'Axel David', 'Delgado Toala', '1302587413', 'Heterosexual', '1982-01-23', 'axeldel@hotmail.com', '0936570314', 6),
('NI04', 'Laddy Mabel', 'Mero Burgos', '1365478201', 'Homosexual', '1999-02-15', 'laddme@hotmail.com', '0965100874', 5),
('NI05', 'Nayeli Damaris', 'Cedeño Bravo', '1315849747', 'Heterosexual', '1995-05-12', 'naye@hotmail.com', '0901598755', 4),
('NI06', 'Branly Andres', 'Yudeh Cedeño', '1315259398', 'Heterosexual', '2001-01-22', 'yudeh22@hotmail.com', '0969097685', 7),
('NI07', 'Andrea Carolina', 'Soledispa Parraga', '1328574109', 'Homosexual', '1997-12-20', 'sole@hotmail.com', '0992107841', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nino`
--

CREATE TABLE `nino` (
  `CEDULA_NINO` char(250) NOT NULL,
  `NOMBRE_NINO` char(250) NOT NULL,
  `APELLIDO_NINO` char(250) NOT NULL,
  `NACIMIENTO_NINO` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nino`
--

INSERT INTO `nino` (`CEDULA_NINO`, `NOMBRE_NINO`, `APELLIDO_NINO`, `NACIMIENTO_NINO`) VALUES
('1247965712', 'Karla Damaris', 'Cedeño Zambrano', '2013-07-09'),
('1300258796', 'Byron Alejandro', 'Bazurto Moreira', '2015-06-25'),
('1302587453', 'Iker Manuel', 'Anchundia Piguave', '2011-06-16'),
('1305411785', 'Manuel Andres', 'Bailon Moreira', '2010-02-25'),
('1306321749', 'Micaela Alejandra', 'Guzman Sornoza', '2020-01-22'),
('1310147851', 'Melanie Mishell', 'Santos Andrade', '2016-08-11'),
('1312036498', 'Marco Polo', 'Anchundia Quijije', '2009-11-01'),
('1319852015', 'Michael Xavier', 'Palma Alvarado', '2017-12-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ocupacion_ninero`
--

CREATE TABLE `ocupacion_ninero` (
  `ID_NINERO` char(250) NOT NULL,
  `AFICION` char(250) NOT NULL,
  `CEDULA_NI` char(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ocupacion_ninero`
--

INSERT INTO `ocupacion_ninero` (`ID_NINERO`, `AFICION`, `CEDULA_NI`) VALUES
('NI01', 'Me gusta leer libros y nadar en la piscina.', '1315147038'),
('NI02', 'Mi afición es jugar futbol.', '1310149624'),
('NI03', 'Mi pasión son los libros de botánica y las películas de Marvel.', '1302587413'),
('NI04', 'Me gusta bailar toda clase de música y la interpretación escénica.', '1365478201'),
('NI05', 'Mi mayor afición es el arte y los deportes acuáticos.', '1315849747'),
('NI06', 'Mi principal afición es la informática, soy un gran programados.', '1315259398'),
('NI07', 'Soy una apasionada de la música y del baile.', '1328574109');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `ID_PAGO` char(250) NOT NULL,
  `METODO_DE_PAGO` char(250) NOT NULL,
  `MONTO_DE_PAGO` int(10) NOT NULL,
  `FECHA_DE_PAGO` date NOT NULL,
  `ID_SOLICITUD` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`ID_PAGO`, `METODO_DE_PAGO`, `MONTO_DE_PAGO`, `FECHA_DE_PAGO`, `ID_SOLICITUD`) VALUES
('PG01', 'Tarjeta de Debito', 8, '2021-02-15', 'SL01'),
('PG02', 'Tarjeta de Credito', 20, '2021-02-17', 'SL02'),
('PG03', 'Tarjeta de Debito', 12, '2021-03-04', 'SL04'),
('PG04', 'Tarjeta de Credito', 28, '2021-03-14', 'SL06'),
('PG05', 'Tarjeta de Credito.', 65, '2021-03-23', 'SL07'),
('PG06', 'Tarjeta de Crédito', 42, '2021-04-01', 'SL09'),
('PG07', 'Tarjeta de Debito', 20, '2021-04-04', 'SL10'),
('PG08', 'Tarjeta de Credito', 32, '2021-04-11', 'SL11'),
('PG09', 'Tarjeta de Credito', 40, '2021-04-23', 'SL13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `referencias`
--

CREATE TABLE `referencias` (
  `REFERENCIA_NI` char(250) NOT NULL,
  `ID_NINERO` char(250) NOT NULL,
  `CEDULA_NI` char(250) NOT NULL,
  `RF_AÑO` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `referencias`
--

INSERT INTO `referencias` (`REFERENCIA_NI`, `ID_NINERO`, `CEDULA_NI`, `RF_AÑO`) VALUES
('Doy fe de que el niñero Axel David Delgado Toala con el numero de cedula 1302587413 es una persona responsable y de buenos valores, capaz de desarrollarse en este trabajo de excelente manera.', 'NI02', '1310149624', 2021),
('Doy fe de que el niñero Branly Andres Yudeh Cedeño con el numero de cedula 1315259398 es una persona responsable y de buenos valores, capaz de desarrollarse en este trabajo de excelente manera.', 'NI02', '1310149624', 2021),
('Doy fe de que la niñera Andrea Carolina Soledispa Parraga con el numero de cedula 1328574109 es una persona responsable y de buenos valores, capaz de desarrollarse en este trabajo de excelente manera.', 'NI02', '1310149624', 2021),
('Doy fe de que la niñera Paula Andrea Cardenas Cedeño con el numero de cedula 1315147038 es una persona responsable y de buenos valores, capaz de desarrollarse en este trabajo de excelente manera.', 'NI02', '1310149624', 2021),
('Puedo recomendar a la niñera Nayeli Damaris Cedeño Bravo con el numero de cedula 1315849747 ya que la considero una persona responsable en su trabajo, así también una persona con increíbles talentos.', 'NI01', '1315147038', 2021),
('Recomiendo a la niñera Laddy Mabel Mero Burgos con el numero de cedula 1365478201 para el trabajo de niñero debido a que es una persona responsable y de buenos valores, capaz de desarrollarse en este trabajo de excelente manera.', 'NI07', '1328574109', 2021);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE `solicitud` (
  `ID_SOLICITUD` char(250) NOT NULL,
  `FECHA_INGRESO` datetime NOT NULL,
  `FECHA_SALIDA` datetime NOT NULL,
  `ESTADO_DE_SOLICITUD` tinyint(1) NOT NULL,
  `OBSERVACIONES` char(250) NOT NULL,
  `ID_CLIENTE` int(10) NOT NULL,
  `CEDULA` char(250) NOT NULL,
  `ID_NINERO` char(250) NOT NULL,
  `CEDULA_NI` char(250) NOT NULL,
  `CALIFICACION` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `solicitud`
--

INSERT INTO `solicitud` (`ID_SOLICITUD`, `FECHA_INGRESO`, `FECHA_SALIDA`, `ESTADO_DE_SOLICITUD`, `OBSERVACIONES`, `ID_CLIENTE`, `CEDULA`, `ID_NINERO`, `CEDULA_NI`, `CALIFICACION`) VALUES
('SL01', '2021-02-15 14:00:00', '2021-02-15 16:00:00', 1, 'Favor llegar puntual.', 1, '1316541239', 'NI05', '1315849747', 5),
('SL02', '2021-02-17 18:00:00', '2021-02-17 22:00:00', 1, 'En caso de que no llegue a la hora establecida por favor quedarse y a lo que llego le pago la hora extra.', 4, '1310851374', 'NI01', '1315147038', 5),
('SL03', '2021-02-25 08:00:00', '2021-02-25 12:00:00', 0, 'Llegar puntual por favor.', 2, '1336520139', 'NI03', '1302587413', 0),
('SL04', '2021-03-04 13:00:00', '2021-03-04 16:00:00', 1, 'Revisar las indicaciones generales. Si se presentan complicaciones no dude en llamarme.', 3, '1318052147', 'NI02', '1310149624', 1),
('SL05', '2021-03-10 20:00:00', '2021-03-11 08:00:00', 0, 'NA', 5, '1312136874', 'NI04', '1365478201', 0),
('SL06', '2021-03-14 19:30:00', '2021-03-14 23:30:00', 1, 'NA.', 6, '1306541202', 'NI06', '1315259398', 5),
('SL07', '2021-03-22 19:00:00', '2021-03-23 08:00:00', 1, 'No acostarlo muy tarde por favor. Tipo 20:00 está bien.', 7, '1312547842', 'NI07', '1328574109', 5),
('SL08', '2021-03-25 12:00:00', '2021-03-25 18:00:00', 0, 'NA.', 8, '1325874142', 'NI05', '1315849747', 0),
('SL09', '2021-04-01 07:00:00', '2021-04-01 14:00:00', 1, 'Llegar puntual.', 5, '1312136874', 'NI03', '1302587413', 1),
('SL10', '2021-04-04 17:00:00', '2021-04-04 21:00:00', 1, 'No dejarlo ver programas en tv violentos y no dejar que suba las escaleras el solo.', 2, '1336520139', 'NI04', '1365478201', 5),
('SL11', '2021-04-11 12:00:00', '2021-04-11 20:00:00', 1, 'Por favor no dejar que consuma muchas galletitas.', 8, '1325874142', 'NI02', '1310149624', 1),
('SL12', '2021-04-18 20:00:00', '2021-04-18 22:00:10', 0, 'NA.', 1, '1316541239', 'NI06', '1315259398', 0),
('SL13', '2021-04-23 13:00:00', '2021-04-23 21:00:00', 1, 'NA.', 3, '1318052147', 'NI04', '1365478201', 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `caracteristica_de_nino`
--
ALTER TABLE `caracteristica_de_nino`
  ADD PRIMARY KEY (`INDICACIONES`),
  ADD KEY `CEDULA_NINO` (`CEDULA_NINO`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`ID_CLIENTE`,`CEDULA`),
  ADD KEY `CEDULA_NINO` (`CEDULA_NINO`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`DIRECCION`),
  ADD KEY `ID_CLIENTE` (`ID_CLIENTE`,`CEDULA`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`ID_FACTURA`),
  ADD KEY `FK_RELATIONSHIP_14` (`ID_PAGO`,`MONTO_DE_PAGO`,`FECHA_DE_PAGO`);

--
-- Indices de la tabla `ninero`
--
ALTER TABLE `ninero`
  ADD PRIMARY KEY (`ID_NINERO`,`CEDULA_NI`);

--
-- Indices de la tabla `nino`
--
ALTER TABLE `nino`
  ADD PRIMARY KEY (`CEDULA_NINO`);

--
-- Indices de la tabla `ocupacion_ninero`
--
ALTER TABLE `ocupacion_ninero`
  ADD PRIMARY KEY (`ID_NINERO`),
  ADD KEY `FK_RELATIONSHIP_5` (`ID_NINERO`,`CEDULA_NI`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`ID_PAGO`,`MONTO_DE_PAGO`,`FECHA_DE_PAGO`),
  ADD KEY `FK_RELATIONSHIP_55` (`ID_SOLICITUD`);

--
-- Indices de la tabla `referencias`
--
ALTER TABLE `referencias`
  ADD PRIMARY KEY (`REFERENCIA_NI`),
  ADD KEY `FK_RELATIONSHIP_4` (`ID_NINERO`,`CEDULA_NI`);

--
-- Indices de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD PRIMARY KEY (`ID_SOLICITUD`),
  ADD KEY `FK_RELATIONSHIP_10` (`ID_CLIENTE`,`CEDULA`),
  ADD KEY `FK_RELATIONSHIP_7` (`ID_NINERO`,`CEDULA_NI`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `caracteristica_de_nino`
--
ALTER TABLE `caracteristica_de_nino`
  ADD CONSTRAINT `FK_RELATIONSHIP_12` FOREIGN KEY (`CEDULA_NINO`) REFERENCES `nino` (`CEDULA_NINO`);

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `FK_RELATIONSHIP_15` FOREIGN KEY (`CEDULA_NINO`) REFERENCES `nino` (`CEDULA_NINO`);

--
-- Filtros para la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD CONSTRAINT `FK_RELATIONSHIP_11` FOREIGN KEY (`ID_CLIENTE`,`CEDULA`) REFERENCES `cliente` (`ID_CLIENTE`, `CEDULA`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `FK_RELATIONSHIP_14` FOREIGN KEY (`ID_PAGO`,`MONTO_DE_PAGO`,`FECHA_DE_PAGO`) REFERENCES `pago` (`ID_PAGO`, `MONTO_DE_PAGO`, `FECHA_DE_PAGO`);

--
-- Filtros para la tabla `ocupacion_ninero`
--
ALTER TABLE `ocupacion_ninero`
  ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`ID_NINERO`,`CEDULA_NI`) REFERENCES `ninero` (`ID_NINERO`, `CEDULA_NI`);

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `FK_RELATIONSHIP_13` FOREIGN KEY (`ID_SOLICITUD`) REFERENCES `solicitud` (`ID_SOLICITUD`),
  ADD CONSTRAINT `FK_RELATIONSHIP_55` FOREIGN KEY (`ID_SOLICITUD`) REFERENCES `solicitud` (`ID_SOLICITUD`);

--
-- Filtros para la tabla `referencias`
--
ALTER TABLE `referencias`
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`ID_NINERO`,`CEDULA_NI`) REFERENCES `ninero` (`ID_NINERO`, `CEDULA_NI`);

--
-- Filtros para la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD CONSTRAINT `FK_RELATIONSHIP_10` FOREIGN KEY (`ID_CLIENTE`,`CEDULA`) REFERENCES `cliente` (`ID_CLIENTE`, `CEDULA`),
  ADD CONSTRAINT `FK_RELATIONSHIP_7` FOREIGN KEY (`ID_NINERO`,`CEDULA_NI`) REFERENCES `ninero` (`ID_NINERO`, `CEDULA_NI`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
