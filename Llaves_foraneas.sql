
--

--
-- Indices de la tabla `caracteristica_de_nino`
--
ALTER TABLE `caracteristica_de_nino`
  ADD PRIMARY KEY (`INDICACIONES`),
  ADD KEY `CEDULA_NINO` (`CEDULA_NINO`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`ID_CLIENTE`,`CEDULA`),
  ADD KEY `CEDULA_NINO` (`CEDULA_NINO`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`DIRECCION`),
  ADD KEY `ID_CLIENTE` (`ID_CLIENTE`,`CEDULA`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`ID_FACTURA`),
  ADD KEY `FK_RELATIONSHIP_14` (`ID_PAGO`,`MONTO_DE_PAGO`,`FECHA_DE_PAGO`);

--
-- Indices de la tabla `ninero`
--
ALTER TABLE `ninero`
  ADD PRIMARY KEY (`ID_NINERO`,`CEDULA_NI`);

--
-- Indices de la tabla `nino`
--
ALTER TABLE `nino`
  ADD PRIMARY KEY (`CEDULA_NINO`);

--
-- Indices de la tabla `ocupacion_ninero`
--
ALTER TABLE `ocupacion_ninero`
  ADD PRIMARY KEY (`ID_NINERO`),
  ADD KEY `FK_RELATIONSHIP_5` (`ID_NINERO`,`CEDULA_NI`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`ID_PAGO`,`MONTO_DE_PAGO`,`FECHA_DE_PAGO`),
  ADD KEY `FK_RELATIONSHIP_55` (`ID_SOLICITUD`);

--
-- Indices de la tabla `referencias`
--
ALTER TABLE `referencias`
  ADD PRIMARY KEY (`REFERENCIA_NI`),
  ADD KEY `FK_RELATIONSHIP_4` (`ID_NINERO`,`CEDULA_NI`);

--
-- Indices de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD PRIMARY KEY (`ID_SOLICITUD`),
  ADD KEY `FK_RELATIONSHIP_10` (`ID_CLIENTE`,`CEDULA`),
  ADD KEY `FK_RELATIONSHIP_7` (`ID_NINERO`,`CEDULA_NI`);

--
